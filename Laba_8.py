from random import randint, uniform
from math import sqrt
from timeit import default_timer


# Функция,которая отбирает точки для заданного радиуса
def point(x_0, y_0, radius, points):
    quantity = 0
    for x, y in points:
        delta = sqrt((x - x_0) ** 2 + (y - y_0) ** 2)
        if delta <= radius:
            quantity += 1
    return quantity


while True:  # Выбор че делать
    try:
        print("Посчитать время точек от 100к до 1млн с шагом 100к?")
        num = int(input('[1]:Нет ---- Ввести свои значения\n'
                        '[2]:Да\n'))
        if num > 2 or num < 1:
            print('Выберите из предложенного')
        else:
            break
    except ValueError:
        print('Ошибка типа данных')

while True:  # Ввод границ графика и рандомных чисел
    try:
        obl_dwn = int(input('Нижняя граница: '))
        obl_up = int(input('Верхняя граница: '))
        if obl_up > obl_dwn:
            break
        else:
            print('Нижняя граница дожна быть меньше верхней')
    except ValueError:
        print('Не правильный формат данных !')

if num == 1:
    while True:  # Ввод индекса точки с радиусом
        try:
            qnt = int(input('Введите количество точек: '))
            if qnt > 0:
                break
            else:
                print('количество точек должно быть больше 0')
        except ValueError:
            print('Не правильный формат данных !')

    while True:
        try:
            index = int(input('Введите индекс точки: '))
            r = float(input('Радиус: '))
            if r >= 0:
                break
            else:
                print('Радиус должен быть => 0')
        except ValueError:
            print('Не правильный формат данных !')

    p = [(uniform(obl_dwn, obl_up),
          uniform(obl_dwn, obl_up)) for i in range(qnt)]

    while True:  # Выбор точки по индексу
        try:
            x_center, y_center = p[index]
            break
        except IndexError:
            print('Индекс не существует')
            exit(-1)

    # Занесение значений в функцию
    qnts = point(x_center, y_center, r, p)
    print('Из', qnt, 'точек входит', qnts, 'точек(ки)')

elif num == 2:
    time = 0
    file = open('time.txt', 'w')  # открытие файла для записи
    for i in range(100000, 1000001, 100000):  # цикл создания точек в заданном радиусе
        r = randint(0, i)
        p = [(uniform(1, i),
              uniform(1, i)) for i in range(i)]
        index = randint(0, i)
        x_center, y_center = p[index]
        qnt = i

        for _ in range(3):
            t_start = default_timer()
            qnts = point(x_center, y_center, r, p)
            time = default_timer() - t_start
        print('Из', qnt, 'точек входит', qnts, 'точек(ки)')

        file.write(str(i) + ' || ' + str(time / 3).replace('.', ',') + '\n')
    file.close()
